//
//  AppDelegate.h
//  IAESTE65
//
//  Created by Patrick Wijerama on 8/7/13.
//  Copyright (c) 2013 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
