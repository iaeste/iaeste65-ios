//
//  main.m
//  IAESTE65
//
//  Created by Patrick Wijerama on 8/7/13.
//  Copyright (c) 2013 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
